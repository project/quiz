<?php

namespace Drupal\quiz\Access;

use Drupal\entity\UncacheableEntityAccessControlHandler;

/**
 * Determines access to Quiz.
 *
 * @ingroup quiz
 */
class QuizAccessControlHandler extends UncacheableEntityAccessControlHandler {

}
