<?php

namespace Drupal\quiz\Entity;

/**
 * Represents a broken question, thus empty.
 */
class QuizQuestionBroken extends QuizQuestion {

}
